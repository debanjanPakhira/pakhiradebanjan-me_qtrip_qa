package qtriptest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {

    public FileInputStream fi;
	public FileOutputStream fo;
	public XSSFWorkbook workbook;
	public XSSFSheet sheet;
	public XSSFRow row;
	public XSSFCell cell;
	public CellStyle style;   
	String path;
	
	public ExcelUtility(String path)
	{
		this.path=path;
	}
		
	public int getRowCount(String sheetName) throws IOException {
		fi=new FileInputStream(path);
		workbook=new XSSFWorkbook(fi);
		sheet=workbook.getSheet(sheetName);
		int rowcount=sheet.getLastRowNum();
		workbook.close();
		fi.close();
		return rowcount;		
	}
	
	public int getCellCount(String sheetName,int rownum) throws IOException
	{
		fi=new FileInputStream(path);
		workbook=new XSSFWorkbook(fi);
		sheet=workbook.getSheet(sheetName);
		row=sheet.getRow(rownum);
		int cellcount=row.getLastCellNum();
		workbook.close();
		fi.close();
		return cellcount;
	}
	
	
	public String getCellData(String sheetName,int rownum,int colnum) throws IOException
	{
		fi=new FileInputStream(path);
		workbook=new XSSFWorkbook(fi);
		sheet=workbook.getSheet(sheetName);
		row=sheet.getRow(rownum);
		cell=row.getCell(colnum);
		
		DataFormatter formatter = new DataFormatter();
		String data;
		try{
		data = formatter.formatCellValue(cell); //Returns the formatted value of a cell as a String regardless of the cell type.
		}
		catch(Exception e)
		{
			data="";
		}
		workbook.close();
		fi.close();
		return data;
	}
	
	// public void setCellData(String sheetName,int rownum,int colnum, boolean status) throws IOException
	// {
	// 	File xlfile=new File(path);
	// 	if(!xlfile.exists())    // If file not exists then create new file
	// 	{
	// 	workbook=new XSSFWorkbook();
	// 	fo=new FileOutputStream(path);
	// 	workbook.write(fo);
	// 	}
				
	// 	fi=new FileInputStream(path);
	// 	workbook=new XSSFWorkbook(fi);
			
	// 	if(workbook.getSheetIndex(sheetName)==-1) // If sheet not exists then create new Sheet
	// 		workbook.createSheet(sheetName);
	// 	sheet=workbook.getSheet(sheetName);
		
	// 	if(sheet.getRow(rownum)==null)   // If row not exists then create new Row
	// 			sheet.createRow(rownum);
	// 	row=sheet.getRow(rownum);

	// 	String[] headers = new String[] { "TestCaseName", "ParameterData", "Status" };
	// 	for (int r=0; r<headers.length; r++) {
	// 		row = sheet.createRow(r);
	// 		row.createCell(0).setCellValue(headers[r]);
	// 	}
		
	// 	cell=row.createCell(colnum);
	// 	cell.setCellValue(status);
	// 	fo=new FileOutputStream(path);
	// 	workbook.write(fo);		
	// 	workbook.close();
	// 	fi.close();
	// 	fo.close();
	// }

	// public void createHeader(String testCaseName, String parameterData, String result){
	// 	String[] headers = new String[] { testCaseName, parameterData, result };

	// 	workbook = new XSSFWorkbook();
	// 	sheet = workbook.createSheet("QTrip Test Results");

	// 	for (int r=0; r<headers.length; r++) {
	// 		row = sheet.createRow(r);
	// 		row.createCell(0).setCellValue(headers[r]);
	// 	}
	// }

	public void writeInToExcel(String filePath, String sheetName, Object[][] dataToWrite) {
		System.out.println("Adding new row to: " + sheetName + "");

		// Creating file object of existing excel file
		File fileName = new File(filePath);

		try {

			FileInputStream file = new FileInputStream(fileName);

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheet(sheetName);				
			
			// Getting the count of existing records
			int rowCount = 1;//0 is for headers

			// Iterating new students to update
			for (Object[] data : dataToWrite) {

				// Creating new row from the next row count
				XSSFRow row = sheet.createRow(rowCount++);
				int columnCount = 0;
				// Iterating student informations
				for (Object info : data) {
					// Creating new cell and setting the value
					XSSFCell cell = row.createCell(columnCount++);
					if (info instanceof String) {
						cell.setCellValue((String) info);
					} else if (info instanceof Integer) {
						cell.setCellValue((Integer) info);
					} else if (info instanceof Double) {
						cell.setCellValue((Double) info);
					}else if (info instanceof Boolean) {
						cell.setCellValue((Boolean) info);
					}
				}
			}
			// Close input stream
			file.close();

			// Crating output stream and writing the updated workbook
			FileOutputStream os = new FileOutputStream(fileName);
			workbook.write(os);

			// Close the workbook and output stream
			workbook.close();
			os.close();

			System.out.println("Excel file has been updated successfully.\n");

		} catch (Exception e) {
			System.err.println("Exception while updating an existing excel file.\n");
			e.printStackTrace();
		}
	}
    
}