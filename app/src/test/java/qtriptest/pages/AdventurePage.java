package qtriptest.pages;

import qtriptest.SeleniumWrapper;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

public class AdventurePage {
    WebDriver driver;

    @FindBy(id = "duration-select")
    WebElement Duration_Filter;

    @FindBy(id = "category-select")
    WebElement Category_Filter;

    @FindBy(id = "search-adventures")
    WebElement search_adventure;

    SeleniumWrapper wrapper = new SeleniumWrapper();

    public AdventurePage(WebDriver driver) {
        // driver.manage().window().maximize();
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public void selectFilter(String DurationFilter, String CategoryFilter) {
        Duration_Filter.click();

        wrapper.click(Duration_Filter, driver);
        //WebElement duration = driver.findElement(By.xpath("//option[text()='" + DurationFilter + "']"));
        By by = By.xpath("//option[text()='" + DurationFilter + "']");
        WebElement duration = wrapper.findElementWithRetry(driver, by, 3);
        wrapper.click(duration, driver);

        Category_Filter.click();
        wrapper.click(Category_Filter, driver);
        //WebElement caregory = driver.findElement(By.xpath("//option[text()='" + CategoryFilter + "']"));
        By by1= By.xpath("//option[text()='" + CategoryFilter + "']");
        WebElement caregory = wrapper.findElementWithRetry(driver, by1, 3);
        wrapper.click(caregory, driver);
    }

    public boolean searchForAdventure(String adventure) throws InterruptedException {
        // search_adventure.clear();
        // search_adventure.sendKeys(adventure);
        wrapper.SendKeys(search_adventure, adventure);
        Thread.sleep(5000);

        // WebDriverWait wait = new WebDriverWait(driver, 10);
        // wait.until(ExpectedConditions.visibilityOfElementLocated(
        //         By.xpath("//h5[contains(text(),'"+ adventure +"')]")));
        WebElement adv = driver.findElement(By.xpath("//h5[contains(text(),'"+ adventure +"')]"));

        return adv.isDisplayed();
    }

    public void clickAdventure(String adventure) throws InterruptedException {
        WebElement adventure_link =
                driver.findElement(By.xpath("//h5[contains(text(),'"+ adventure +"')]"));
        adventure_link.click();
        Thread.sleep(5000);
    }

    public boolean expectedFilterResult(String result) {
        List<WebElement> filterResult = driver.findElements(By.className("category-banner"));

        if (filterResult.size() == Integer.parseInt(result)) {
            return true;
        }
        return false;


    }

    public void clearFilters() {
        List<WebElement> filters = driver.findElements(By.xpath("//div[@class='ms-3']"));
        for (WebElement filter : filters) {
            filter.click();
        }
    }

}
