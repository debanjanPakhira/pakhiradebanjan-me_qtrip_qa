package qtriptest.pages;

import qtriptest.SeleniumWrapper;
import java.sql.Timestamp;
//import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {
    WebDriver driver;
    public String lastGeneratedUsername;
    String url = "https://qtripdynamic-qa-frontend.vercel.app/pages/register/";

    @FindBy(name="email")
    WebElement email_text_box;

    @FindBy(name="password")
    WebElement psssword_txt_box;

    @FindBy(name="confirmpassword")
    WebElement confirmPassword_txt_box;

    @FindBy(xpath="//button[text()='Register Now']")
    WebElement registerNow;

    public RegisterPage(WebDriver driver){
        driver.manage().window().maximize();
        this.driver=driver;
        // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }


    public boolean registerNewUser(String username, String password, boolean makeUsernameDynamic) throws InterruptedException{
        // if(!driver.getCurrentUrl().equals("https://qtripdynamic-qa-frontend.vercel.app/pages/register/")){
        //     driver.get("https://qtripdynamic-qa-frontend.vercel.app/pages/register/");
        // }
        SeleniumWrapper wrapper = new SeleniumWrapper();
        wrapper.NavigatetoURL(driver, url);

        String usernameDynamic;

        Timestamp time = new Timestamp(System.currentTimeMillis());
        if(makeUsernameDynamic)
        usernameDynamic=time.getTime()+username ;
        else
        usernameDynamic=username;

        //Thread.sleep(3000);
        // email_text_box.click();
        // email_text_box.clear();
        // email_text_box.sendKeys(usernameDynamic);

        wrapper.SendKeys(email_text_box, usernameDynamic);

        //Thread.sleep(3000);
        // psssword_txt_box.click();
        // psssword_txt_box.clear();
        // psssword_txt_box.sendKeys(password);

        wrapper.SendKeys(psssword_txt_box, password);

        
        //Thread.sleep(3000);
        // confirmPassword_txt_box.click();
        // confirmPassword_txt_box.clear();
        // confirmPassword_txt_box.sendKeys(password);

        wrapper.SendKeys(confirmPassword_txt_box, password);

        //registerNow.click();
        wrapper.click(registerNow, driver);
        
        lastGeneratedUsername=usernameDynamic;

        //Thread.sleep(3000);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlToBe("https://qtripdynamic-qa-frontend.vercel.app/pages/login"));
        
        return this.driver.getCurrentUrl().endsWith("/login");

    }
}

