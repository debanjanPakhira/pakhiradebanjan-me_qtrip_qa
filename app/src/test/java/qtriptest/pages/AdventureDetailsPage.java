
package qtriptest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdventureDetailsPage {
    WebDriver driver;

    @FindBy(name="name")
    WebElement name_txt_box;

    @FindBy(name="date")
    WebElement date;

    @FindBy(name="person")
    WebElement count_person;

    @FindBy(xpath="//button[text()='Reserve']")
    WebElement reserve_button;

    public AdventureDetailsPage(WebDriver driver){
          this.driver= driver;
          PageFactory.initElements(new AjaxElementLocatorFactory(driver,10), this);
    }

    public void enter_Name_Date_Count(String name, String Date, String count){
        name_txt_box.clear();
        name_txt_box.sendKeys(name);

        date.clear();
        date.sendKeys(Date);

        count_person.clear();
        count_person.sendKeys(count);

    }

    public boolean clickReserve(){
        reserve_button.click();
        WebDriverWait wait = new WebDriverWait(driver,10);
        WebElement greetings_txt =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Greetings!')]")));

        

        if(greetings_txt.isDisplayed())
        return true;
        else
        return false;
    }


}