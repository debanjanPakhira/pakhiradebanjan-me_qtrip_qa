
package qtriptest.pages;

import qtriptest.SeleniumWrapper;
//import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HistoryPage {
    WebDriver driver;

    @FindBy(xpath="//strong[text()='here']")
    WebElement reservation;

    @FindBy(xpath="//tbody[@id='reservation-table']/tr/th")
    WebElement reservation_ids;

    @FindBy(xpath="//button[text()='Cancel']")
    WebElement cancel_button;

    @FindBy(xpath="//a[text()='Home']")
    WebElement home_button;

    SeleniumWrapper wrapper = new SeleniumWrapper();
    

    public HistoryPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver,10), this);
    }


    public void clickReservation(){
        wrapper.click(reservation, driver);
       // reservation.click();

        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.urlToBe("https://qtripdynamic-qa-frontend.vercel.app/pages/adventures/reservations/"));

    }

    public String getReservationId(){
        return "The reservation ID is: " + reservation_ids.getText();
        
    }

    public boolean isReservtionIdNull(String ID){
        if(ID.length()==0)
        return true;
        else
        return false;
    }

    public boolean clickCancel() throws InterruptedException{
        //cancel_button.click();
        wrapper.click(cancel_button, driver);
        Thread.sleep(3000);
        // WebDriverWait wait = new WebDriverWait(driver,10);
        // boolean status= wait.until(ExpectedConditions.invisibilityOf(reservation_ids));
        //wait.until(ExpectedConditions.invisibilityOf(reservation_ids));
        
        try{
           return !(reservation_ids.isDisplayed());

           } catch (Exception e) {
            
            return true;
        }
    }

    public void clickHomeButton() throws InterruptedException{
        //home_button.click();
        wrapper.click(home_button, driver);
        Thread.sleep(1000);
    }
    
    


}