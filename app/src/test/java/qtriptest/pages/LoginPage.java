package qtriptest.pages;

import qtriptest.SeleniumWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    WebDriver driver;
    String url ="https://qtripdynamic-qa-frontend.vercel.app/pages/login";

    @FindBy(name="email")
    WebElement email_txt_box;


    @FindBy(name="password")
    WebElement password_txt_box;

    @FindBy(xpath="//button[text()='Login to QTrip']")
    WebElement loginButton;

    @FindBy(xpath="//div[text()='Logout']")
    WebElement logoutButton;

    SeleniumWrapper wrapper = new SeleniumWrapper();

public LoginPage(WebDriver driver){
        
        driver.manage().window().maximize();
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }


public void NavigateToLoginPage(WebDriver driver){
        // if(!this.driver.getCurrentUrl().equals(url)){
        //     driver.get(url);
        
        wrapper.NavigatetoURL(driver, url);
        }


public boolean PerformLogin(String username, String password) throws InterruptedException{
    // email_txt_box.sendKeys(username);
    // password_txt_box.sendKeys(password);
    // loginButton.click();
    wrapper.SendKeys(email_txt_box, username);
    wrapper.SendKeys(password_txt_box, password);
    wrapper.click(loginButton, driver);
    
    
    Thread.sleep(2000);
    driver.switchTo().alert().accept();
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.urlToBe("https://qtripdynamic-qa-frontend.vercel.app/"));
    return isUserLoggedIn(driver);
}

public boolean isUserLoggedIn(WebDriver driver){
    if(driver.getCurrentUrl().equals("https://qtripdynamic-qa-frontend.vercel.app/"))
    return true;
    else
    return false;
    
}

public boolean performLogout(){
    //logoutButton.click();
    wrapper.click(logoutButton, driver);
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='navbarNavDropdown']/ul/li[4]/a")));
    WebElement element = driver.findElement(By.xpath("//*[@id='navbarNavDropdown']/ul/li[4]/a"));
    if(element.isDisplayed())
    return true;
    else
    return false;

}
}
