package qtriptest.pages;

import qtriptest.SeleniumWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    WebDriver driver;
    
    @FindBy(id="autocomplete")
    WebElement search_box;

    @FindBy(xpath="//h5[text()='No City found']")
    WebElement noCityTxt;

    SeleniumWrapper wrapper = new SeleniumWrapper();

    public HomePage(WebDriver driver){
        driver.manage().window().maximize();
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver,10), this);
    }

    public void searchCity(String city) throws InterruptedException{
        // search_box.clear();
        // search_box.sendKeys(city);
        wrapper.SendKeys(search_box, city);
        Thread.sleep(2000);
        
        // Thread.sleep(2000);
        // search_box.click();
        // search_box.clear();
        // search_box.sendKeys(city);
        
        
    }

    public boolean isNoCityTextDisplayed(){
        if(noCityTxt.isDisplayed())
        return true;
        else
        return false;
    }

    public boolean isAutoCompleteCityDisplayed(String city) throws InterruptedException{

        WebElement element = driver.findElement(By.xpath("//li[contains(text(), '" +city+ "')]"));
        Thread.sleep(3000);
        if(element.isDisplayed())
        return true;
        else
        return false;
    }

    public void clickCity(String city){
       // WebElement element = driver.findElement(By.xpath("//li[contains(text(), '" +city+ "')]"));

        By by= By.xpath("//li[contains(text(), '" +city+ "')]");

        WebElement element = wrapper.findElementWithRetry(driver, by, 3);
        element.click();

        WebDriverWait wait1 = new WebDriverWait(driver,10);
        wait1.until(ExpectedConditions.urlContains("/adventures/"));

    }

}
