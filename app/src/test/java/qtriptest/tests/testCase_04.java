package qtriptest.tests;

import qtriptest.DP;
import qtriptest.DriverSingleton;
import qtriptest.ReportSingleton;
import qtriptest.pages.AdventureDetailsPage;
import qtriptest.pages.AdventurePage;
import qtriptest.pages.HistoryPage;
import qtriptest.pages.HomePage;
import qtriptest.pages.LoginPage;
import qtriptest.pages.RegisterPage;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testCase_04 {

    RemoteWebDriver driver;
    static String last_generated_username;
    DriverSingleton driver_singleton;
    ReportSingleton report_singleton;
    ExtentReports report;
    ExtentTest test;

    @BeforeTest(alwaysRun = true)
    public void createDriver() throws MalformedURLException {
        driver_singleton = DriverSingleton.get_Driver_singleton_Instance();
        driver=DriverSingleton.get_Driver();
        report_singleton= ReportSingleton.get_instance_of_ReportSingleton();
        report = ReportSingleton.get_Report();
        test= report.startTest("TestCase04");
    }

    @Test(dataProvider = "dataProvider", dataProviderClass = DP.class, priority = 4, groups={"Reliability Flow"})
    public void TestCase04(Object obj, String username, String password, String dataset_1, String dataset_2, String dataset_3) throws InterruptedException{
        boolean status;
        RegisterPage registration = new RegisterPage(driver);
        status= registration.registerNewUser(username, password, true);
        Assert.assertTrue(status, "Failed to register new user");
        ReportSingleton.get_test_log(test, status, "New user registered successfully");
        last_generated_username = registration.lastGeneratedUsername;

        LoginPage login = new LoginPage(driver);
        login.NavigateToLoginPage(driver);
        status = login.PerformLogin(last_generated_username, password);
        Assert.assertTrue(status,"Failed to login");
        ReportSingleton.get_test_log(test, status, "Newly registered login successfully");



        String[] dataSet_1 = dataset_1.split(";");
        String[] dataSet_2 = dataset_2.split(";");
        String[] dataSet_3 = dataset_3.split(";");

        List<String[]> datasets = Arrays.asList(dataSet_1, dataSet_2, dataSet_3);
        

        for(String[] dataset:datasets){

        HomePage homepage = new HomePage(driver);
        Thread.sleep(3000);
        homepage.searchCity(dataset[0]);
        status=homepage.isAutoCompleteCityDisplayed(dataset[0]);
        Assert.assertTrue(status, "City-"+ dataset[0]+" autocomplete text is not displayed");
        ReportSingleton.get_test_log(test, status, "autocomplete text is displayed successfully");
        homepage.clickCity(dataset[0]);

        AdventurePage adventure = new AdventurePage(driver);
        adventure.clearFilters();
        status= adventure.searchForAdventure(dataset[1]);
        Assert.assertTrue(status, "failed to get the adventure");
        ReportSingleton.get_test_log(test, status, "Adventure is shown successfully");
        Thread.sleep(3000);
        adventure.clickAdventure(dataset[1]);

        AdventureDetailsPage advDetails = new AdventureDetailsPage(driver);
        advDetails.enter_Name_Date_Count(dataset[2], dataset[3], dataset[4]);
        status=advDetails.clickReserve();
        Assert.assertTrue(status, "Reservation failed");
        ReportSingleton.get_test_log(test, status, "Reservation is done successfully");


        HistoryPage historyPage = new HistoryPage(driver);
        historyPage.clickReservation();
        Thread.sleep(3000);
        String reservation_id = historyPage.getReservationId();
        System.out.println("The reservation ID is: "+reservation_id);
        status = historyPage.isReservtionIdNull(reservation_id);
        Assert.assertTrue(!status, "Reservation ID is null");
        ReportSingleton.get_test_log(test, !status, "Reservation ID is checked successfully");
        historyPage.clickHomeButton();


        }
        


    }

    @AfterSuite
    public void quitDriver(){
        driver.quit();
        report.endTest(test);
        report.flush();
        }
}
