package qtriptest.tests;

import qtriptest.DP;
import qtriptest.DriverSingleton;
import qtriptest.ReportSingleton;
import qtriptest.pages.LoginPage;
import qtriptest.pages.RegisterPage;
import java.net.MalformedURLException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testCase_01 {
    RemoteWebDriver driver;
    static String last_generated_username;
    DriverSingleton driver_singleton;
    ReportSingleton report_singleton;
    ExtentReports report;
    ExtentTest test;


    @BeforeTest(alwaysRun = true)
    public void createDriver() throws MalformedURLException {
        driver_singleton=DriverSingleton.get_Driver_singleton_Instance();
        driver = DriverSingleton.get_Driver();
        report_singleton= ReportSingleton.get_instance_of_ReportSingleton();
        report = ReportSingleton.get_Report();
        test= report.startTest("TestCase01");
    }
    
    @Test(dataProvider = "dataProvider", dataProviderClass=DP.class, priority = 1, groups={"Login Flow"})
    public void TestCase01 (Object obj, String Username, String Password) throws InterruptedException{
        boolean status;
        RegisterPage registration = new RegisterPage(driver);
        status = registration.registerNewUser(Username, Password, true);
        Assert.assertTrue(status, "Failed to register new user");
        ReportSingleton.get_test_log(test, status, "New user registered sucessfully");

        last_generated_username = registration.lastGeneratedUsername;

        LoginPage login = new LoginPage(driver);
        login.NavigateToLoginPage(driver);
        status = login.PerformLogin(last_generated_username, Password);
        Assert.assertTrue(status,"Failed to login");
        ReportSingleton.get_test_log(test, status, "Newly registerd user login successfully");
         
        status=login.performLogout();
        Assert.assertTrue(status,"Failed to logout");
        ReportSingleton.get_test_log(test, status, "Login-logout functionality tested successfully");
    }

    

    @AfterTest(alwaysRun = true)
    public void quitDriver(){
        driver.quit();
        report.endTest(test);
        report.flush();

    }
    


}
