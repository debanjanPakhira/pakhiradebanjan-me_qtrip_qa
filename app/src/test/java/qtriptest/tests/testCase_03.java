package qtriptest.tests;

import qtriptest.DP;
import qtriptest.DriverSingleton;
import qtriptest.ReportSingleton;
import qtriptest.pages.AdventureDetailsPage;
import qtriptest.pages.AdventurePage;
import qtriptest.pages.HistoryPage;
import qtriptest.pages.HomePage;
import qtriptest.pages.LoginPage;
import qtriptest.pages.RegisterPage;
import java.net.MalformedURLException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testCase_03 {

    RemoteWebDriver driver;
    static String last_generated_username;
    DriverSingleton driver_singleton;
    ReportSingleton report_singleton;
    ExtentReports report;
    ExtentTest test;



    @BeforeTest(alwaysRun=true)
    public void createDriver() throws MalformedURLException {
        driver_singleton = DriverSingleton.get_Driver_singleton_Instance();
        driver=DriverSingleton.get_Driver();
        report_singleton= ReportSingleton.get_instance_of_ReportSingleton();
        report = ReportSingleton.get_Report();
        test= report.startTest("TestCase03");
    }

    @Test(dataProvider="dataProvider", dataProviderClass=DP.class,priority = 3, groups={"Booking and Cancellation Flow"})
    public void TestCase03(Object obj, String NewUserName, String Password, String SearchCity, String AdventureName, String GuestName, String Date, String count) throws InterruptedException{
        boolean status;
        RegisterPage registration = new RegisterPage(driver);
        status= registration.registerNewUser(NewUserName, Password, true);
        Assert.assertTrue(status, "Failed to register new user");
        ReportSingleton.get_test_log(test, status, "New user registered successfully");

        last_generated_username = registration.lastGeneratedUsername;


        LoginPage login = new LoginPage(driver);
        login.NavigateToLoginPage(driver);
        status = login.PerformLogin(last_generated_username, Password);
        Assert.assertTrue(status,"Failed to login");
        ReportSingleton.get_test_log(test, status, "Newly registered user login successfully");

        HomePage homepage = new HomePage(driver);
        Thread.sleep(3000);
        homepage.searchCity(SearchCity);
        status=homepage.isAutoCompleteCityDisplayed(SearchCity);
        Assert.assertTrue(status, "City autocomplete is not displayed");
        ReportSingleton.get_test_log(test, status, "City autocomplete is displayed successfully");
        homepage.clickCity(SearchCity);

        AdventurePage adventure = new AdventurePage(driver);
        adventure.clearFilters();
        status=adventure.searchForAdventure(AdventureName);
        Assert.assertTrue(status, "failed to get the adventure");
        ReportSingleton.get_test_log(test, status, "Adventure is shown successfully");
        Thread.sleep(3000);
        adventure.clickAdventure(AdventureName);

        AdventureDetailsPage advDetails = new AdventureDetailsPage(driver);
        advDetails.enter_Name_Date_Count(GuestName, Date, count);
        status=advDetails.clickReserve();
        Assert.assertTrue(status, "Reservation failed");
        ReportSingleton.get_test_log(test, status, "Reservation is done successfully");

        HistoryPage historyPage = new HistoryPage(driver);
        historyPage.clickReservation();
        Thread.sleep(3000);
        String reservation_id = historyPage.getReservationId();
        System.out.println("The reservation ID is: "+reservation_id);
        status = historyPage.isReservtionIdNull(reservation_id);

        Assert.assertTrue(!status, "Reservation ID is null");
        status=historyPage.clickCancel();
        Assert.assertTrue(status,"Failed to cancel reservation");
        ReportSingleton.get_test_log(test, status, "Reservation is cancelled successfully");

    }

    @AfterTest
    public void quitDriver(){
        driver.quit();
        report.endTest(test);
        report.flush();
        }
    
    }
