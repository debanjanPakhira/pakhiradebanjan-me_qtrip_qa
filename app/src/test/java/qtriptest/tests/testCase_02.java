package qtriptest.tests;

import qtriptest.DP;
import qtriptest.DriverSingleton;
import qtriptest.ReportSingleton;
import qtriptest.pages.AdventurePage;
import qtriptest.pages.HomePage;
import qtriptest.pages.LoginPage;
import qtriptest.pages.RegisterPage;
import java.net.MalformedURLException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class testCase_02 {
    RemoteWebDriver driver;
    static String last_generated_username;
    DriverSingleton driver_singleton;
    ReportSingleton report_singleton;
    ExtentReports report;
    ExtentTest test;

    
    @BeforeTest(alwaysRun = true)
    public void createDriver() throws MalformedURLException {
        driver_singleton = DriverSingleton.get_Driver_singleton_Instance();
        driver=DriverSingleton.get_Driver();
        report_singleton= ReportSingleton.get_instance_of_ReportSingleton();
        report = ReportSingleton.get_Report();
        test= report.startTest("TestCase02");
    }
    


    @Test(dataProvider = "dataProvider", dataProviderClass=DP.class, priority = 2, groups={"Search and Filter flow"})
    public void TestCase02(Object obj1, String CityName, String Category_Filter, String DurationFilter, String ExpectedFilteredResults, String ExpectedUnFilteredResults) throws InterruptedException{
        boolean status;
        RegisterPage registration = new RegisterPage(driver);
        status=registration.registerNewUser("testUser@test.com", "123xyz@?#", true);
        Assert.assertTrue(status,"Failed to register new user");
        ReportSingleton.get_test_log(test, status, "New user registered successfully");

        last_generated_username = registration.lastGeneratedUsername;

        LoginPage login = new LoginPage(driver);
        login.NavigateToLoginPage(driver);
        status = login.PerformLogin(last_generated_username, "123xyz@?#");
        Assert.assertTrue(status,"Failed to login");
        ReportSingleton.get_test_log(test, status, "Newly registered user login successfully");
         
        HomePage homepage = new HomePage(driver);
        Thread.sleep(3000);
        homepage.searchCity(CityName);
        homepage.clickCity(CityName);
       
        AdventurePage adventurePage = new AdventurePage(driver);
        Thread.sleep(3000);
        adventurePage.clearFilters();

        adventurePage.selectFilter(DurationFilter, Category_Filter);
        Thread.sleep(3000);
        // int Filter_result = Integer.parseInt(ExpectedFilteredResults);
        status=adventurePage.expectedFilterResult(ExpectedFilteredResults);
        Thread.sleep(3000);
        Assert.assertTrue(status, "Expected filtered search reasult does not match");
        ReportSingleton.get_test_log(test, status, "Expected filtered search reasult match");

        adventurePage.clearFilters();
        status=adventurePage.expectedFilterResult(ExpectedUnFilteredResults);
        Assert.assertTrue(status, "Expected unfiltered search reasult does not match");
        ReportSingleton.get_test_log(test, status, "Expected unfiltered search reasult match");

}


    @AfterTest
    public void quitDriver(){
        driver.quit();
        report.endTest(test);
        report.flush();
            
    }

}
