package qtriptest;

import java.io.File;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ReportSingleton {
    //private static ExtentTest test=null;
    private static ExtentReports report=null;
    private static ReportSingleton ReportSingleton_instance=null;

    private ReportSingleton (){

          report = new ExtentReports(System.getProperty("user.dir")+"/ExtentReportResults.html"); 
          report.loadConfig(new File(System.getProperty("user.dir")+"/src/test/java/qtriptest/ReportConfiguration.xml"));
          //test = report.startTest(Test_Case_Class);
          
    }

    public static ReportSingleton get_instance_of_ReportSingleton(){
        if(ReportSingleton_instance== null){
            ReportSingleton_instance = new ReportSingleton();
        }
        return ReportSingleton_instance;
    }

    public static void  get_test_log(ExtentTest test, boolean status, String message){

        if(status){
            test.log(LogStatus.PASS, message);
        }else{
            test.log(LogStatus.FAIL, "Failed to validate: "+message);
        }

    }

    public static ExtentReports get_Report(){
        return report;

        // report.endTest(test);
        // report.flush();

    }

    

}