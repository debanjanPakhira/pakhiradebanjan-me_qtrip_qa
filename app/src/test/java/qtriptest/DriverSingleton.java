package qtriptest;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class DriverSingleton {
    private static RemoteWebDriver driverInstance=null;
    private static DriverSingleton Driver_singleton_Instance = null;

    private DriverSingleton() throws MalformedURLException{
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(BrowserType.CHROME);
        driverInstance = new RemoteWebDriver(new URL("http://localhost:8082/wd/hub"), capabilities);
    }


    public static DriverSingleton get_Driver_singleton_Instance() throws MalformedURLException{
        if(Driver_singleton_Instance == null){
            Driver_singleton_Instance = new DriverSingleton();
        }
        return Driver_singleton_Instance;
    }

    public static RemoteWebDriver get_Driver(){
        return driverInstance;
    }
}