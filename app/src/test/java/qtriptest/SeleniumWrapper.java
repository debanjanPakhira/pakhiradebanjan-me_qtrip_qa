package qtriptest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SeleniumWrapper {
    public boolean click(WebElement elementToClick, WebDriver driver) {
        if(elementToClick.isDisplayed()){
            Actions actions = new Actions(driver);
            actions.moveToElement(elementToClick);
            elementToClick.click();
            return true;
        }
        return false;
        
    }

    public boolean SendKeys(WebElement inputBox, String text){
        if(inputBox.isDisplayed()){
            inputBox.clear();
            inputBox.sendKeys(text);
            return true;
        }
        return false;

    }

    public boolean NavigatetoURL(WebDriver driver, String url){
        if(!driver.getCurrentUrl().equals(url)){
            driver.get(url);
        }

        return driver.getCurrentUrl().equals(url);
        
    }

    public WebElement findElementWithRetry(WebDriver driver , By by , int retryCount ){
        for(int i =0; i<retryCount; i++){
            try{
                return driver.findElement(by);
            }catch(Exception e){
                System.out.println("Webelement can not be found in iteration: "+i);
            }
        }

        return driver.findElement(by);
    }
}
