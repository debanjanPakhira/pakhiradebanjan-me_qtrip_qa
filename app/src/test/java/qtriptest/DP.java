package qtriptest;


import java.io.IOException;
import java.lang.reflect.Method;
import org.testng.annotations.DataProvider;

public class DP {
        @DataProvider(name = "dataProvider")
        public Object[][] dpMethod(Method m) throws IOException {
                String filepath = System.getProperty("user.dir")+ "/src/test/resources/DatasetsforQTrip.xlsx";
                ExcelUtility excelUtility = new ExcelUtility(filepath);

                int totalRows = excelUtility.getRowCount(m.getName());
                int totalColumns = excelUtility.getCellCount(m.getName(), 1);
                String[][] testData = new String[totalRows][totalColumns];

                for(int i=1; i<=totalRows; i++){
                        for(int j=0; j<totalColumns; j++){
                    
                                testData[i-1][j]= excelUtility.getCellData(m.getName(), i, j);
                        }
                }

         return testData;

 
         
        }
}
